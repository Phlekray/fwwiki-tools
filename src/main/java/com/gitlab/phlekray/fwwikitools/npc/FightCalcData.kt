package com.gitlab.phlekray.fwwikitools.npc

/**
 * Prints fight calculation data in the same format as npclist2fightcalcdata.pl.
 */
fun main() {
    parseNpcs().forEach { println("${it.name};${it.attack};${it.lifepoints}") }
}