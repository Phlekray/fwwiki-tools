package com.gitlab.phlekray.fwwikitools.npc

import com.google.gson.JsonParser
import java.io.InputStreamReader
import java.net.URL
import java.util.*
import java.util.stream.Collectors
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.streams.toList

const val NPC_PAGES_QUERY = "https://www.fwwiki.de/api.php?action=query&format=json&cmlimit=100&cmsort=sortkey&cmstartsortkey=0&list=categorymembers&cmtitle=Kategorie:NPCs&cmprop=ids%7Ctitle%7Ctype&cmcontinue=CONTINUE"
const val NPC_PAGE_QUERY = "https://www.fwwiki.de/api.php?action=query&format=json&pageids=PAGE_ID&prop=revisions&rvprop=content"

/**
 * Prints a list of all NPCs from the fwwiki, using the same format as npclist.php.
 */
fun main() {
    parseNpcs().forEach { println(it.toNpcListString()) }
}

private fun Npc.toNpcListString(): String {
    return listOf(
            name,
            attack,
            lifepoints,
            xp,
            gm,
            listOf(regions, positions.map { "" + it.x + "," + it.y }).flatten().joinToString("/"),
            items.joinToString("/"),
            picture,
            author,
            if (attackable) 0 else 1,
            type
    ).joinToString(";")
}

/**
 * Parses all npc pages in the fwwiki.
 */
fun parseNpcs(): List<Npc> {
    val npcs = ArrayList<Npc>()

    for (npcPageId in npcPageIds()) {
        val npcPage = npcPage(npcPageId)
        npcs += newNpc(npcPage.title, npcPage.layout, npcPage.positions)

        for (variation in npcPage.variations) {
            val layoutVariation = HashMap(npcPage.layout)
            if (variation.containsKey("A")) {
                layoutVariation["Stärke"] = variation["A"]
            }
            if (variation.containsKey("LP")) {
                layoutVariation["Lebenspunkte"] = variation["LP"]
            }
            if (variation.containsKey("Gold")) {
                layoutVariation["Gold"] = variation["Gold"]
            }
            if (variation.containsKey("Drop")) {
                layoutVariation["Items"] = variation["Drop"]
            }
            npcs += newNpc(npcPage.title, layoutVariation, npcPage.positions)
        }
    }

    return npcs
}

private fun npcPageIds(): List<String> {
    val pageIds = ArrayList<String>()

    var continueToken = ""
    do {
        val reader = InputStreamReader(URL(NPC_PAGES_QUERY.replace("CONTINUE", continueToken)).openStream())
        val root = JsonParser.parseReader(reader).asJsonObject
        val npcPages = root.get("query").asJsonObject.get("categorymembers").asJsonArray
        pageIds += npcPages
                .filter { it.asJsonObject.getAsJsonPrimitive("type").asString == "page" }
                .map { it.asJsonObject.getAsJsonPrimitive("pageid").asString }
        continueToken = if (root.has("continue")) {
            root.get("continue").asJsonObject.get("cmcontinue").asString
        } else {
            ""
        }
    } while (continueToken.isNotEmpty())

    return pageIds
}

private class NpcPage(val title: String, val layout: Map<String, String>, val variations: List<Map<String, String>>,
                      val positions: List<Position>) {
    class Position(val x: String, val y: String)
}
private fun npcPage(pageId: String): NpcPage {
    val reader = InputStreamReader(URL(NPC_PAGE_QUERY.replace("PAGE_ID", pageId)).openStream())
    val root = JsonParser.parseReader(reader).asJsonObject
    val page = root.get("query").asJsonObject.get("pages").asJsonObject.get(pageId).asJsonObject
    val templates = extractTemplates(page.get("revisions").asJsonArray.get(0).asJsonObject.get("*").asString)

    val layout = templates.stream()
            .filter { it.startsWith("NPC/Layout") }
            .map { splitIntoMap(it) }
            .findAny().orElse(HashMap())

    val variations = templates.stream()
            .filter { it.startsWith("NPC/Ausnahme") }
            .map { splitIntoMap(it) }
            .toList()

    val positions = templates.stream()
            .filter { it.contains(Regex("\\AFeld\\s*\\|")) }
            .map { splitIntoMap(it) }
            .filter { it.containsKey("3") && it.containsKey("4") }
            .map { NpcPage.Position(it["3"] ?: error(""), it["4"] ?: error("")) }
            .toList().toMutableList()

    val mapBegin = templates.indexOfFirst { it.startsWith("Feld/Beginn") }
    val mapEnd = templates.indexOfFirst { it.startsWith("Feld/Ende") }
    if (mapBegin >= 0 && mapEnd >= 0 && mapBegin < mapEnd) {
        val xCoords = ArrayList<String>()
        for (i in mapBegin until templates.indexOfFirst { it.startsWith("Karte/NeueZeile") }) {
            if (templates[i].startsWith("Karte/Koord")) {
                xCoords += splitIntoMap(templates[i]).getOrDefault("1", "0")
            }
        }

        var xIndex = 0
        var yCoord = "0"
        for (i in templates.indexOfFirst { it.startsWith("Karte/NeueZeile") } .. mapEnd) {
            when {
                templates[i].startsWith("Karte/NeueZeile") -> {
                    xIndex = 0
                }
                templates[i].startsWith("Karte/Feld") -> {
                    positions += NpcPage.Position(xCoords.getOrElse(xIndex) {"0"}, yCoord)
                    xIndex++
                }
                templates[i].startsWith("Karte/Leer") -> {
                    xIndex++
                }
                templates[i].startsWith("Karte/Koord") -> {
                    yCoord = splitIntoMap(templates[i]).getOrDefault("1", "0")
                }
            }
        }
    }

    return NpcPage(page.get("title").asString, layout, variations, positions)
}

private fun extractTemplates(pageContent: String) : List<String> {
    val templates = ArrayList<String>()

    var i = 0
    val stack: Stack<Char> = Stack()
    while (i < pageContent.length) {
        if (i < pageContent.length - 1 && pageContent[i] == '}' && pageContent[i + 1] == '}') {
            var template = ""
            var templateBegin = false
            while (!stack.isEmpty() && !templateBegin) {
                val current = stack.pop()
                if (current == '{' && stack.peek() == '{') {
                    templateBegin = true
                    stack.pop()
                } else {
                    template = current + template
                }
            }
            templates += template
            i += 2

        } else {
            stack.push(pageContent[i])
            i++
        }
    }
    templates += stack.toList().reversed().joinToString()

    return templates
}

private fun splitIntoMap(template: String) : Map<String, String> {
    val splitTemplate = template
            .replace(Regex("\\[\\[.*?]]")) { match -> // separator in [link|linkText] is link specific
                match.value.replaceFirst("|", "\$§\$§\$§")
            }
            .replace("{{", "|").replace("}}", "|") // just template separators
            .split("|")
            .stream().map { it.replace("\$§\$§\$§", "|") }.toList()
    val mappedTemplate = HashMap<String, String>()
    for (i in splitTemplate.indices) {
        if (splitTemplate[i].contains("=")) { // first '=' separates content key and value
            val key = splitTemplate[i].split("=")[0].trim().replace(Regex("\\v"), " ")
            val value = splitTemplate[i].split("=")[1].trim().replace(Regex("\\v"), " ")
            if (value != "none") {
                mappedTemplate[key] = value
            }
        } else {
            mappedTemplate[i.toString()] = splitTemplate[i].trim().replace(Regex("\\v"), " ")
        }
    }
    return mappedTemplate
}

class Npc(val name: String, val type: String, val attackable: Boolean, val attack: Int, val lifepoints: Int,
          val xp: Int, val gm: Int, val items: List<String>, val regions: List<String>, val positions: List<Position>,
          val picture: String, val author: String) {
   class Position(val x: Int, val y: Int)
}
private fun newNpc(pageTitle: String, npcLayout: Map<String, String>, pagePositions: List<NpcPage.Position>) : Npc {
    val name = npcLayout["Name"] ?: pageTitle
    val type = parseType(npcLayout["Typ"])
    val attackable = !npcLayout.containsKey("unangreifbar")
    val attack = parseInt(npcLayout["Stärke"])
    val lifepoints = parseInt(npcLayout["Lebenspunkte"])
    val xp = parseInt(npcLayout["XP"])
    val gm = parseInt(npcLayout["Gold"])
    val items = parseLinkList(npcLayout["Items"], false)
    val regions = parseLinkList(npcLayout["Vorkommen"], true)
    val positions = pagePositions.map { Npc.Position(parseInt(it.x), parseInt(it.y)) }.toList()
    val picture = npcLayout["Bild"] ?: ""
    val author = npcLayout["BildAutor"] ?: ""
    return Npc(name, type, attackable, attack, lifepoints, xp, gm, items, regions, positions, picture, author)
}

private fun parseType(templateValue: String?): String {
    return when ((templateValue ?: "").replace("-", "").toLowerCase()) {
        "gruppe", "gruppen", "gruppennpc" -> "gruppennpc"
        "unique", "uniquenpc"-> "uniquenpc"
        "resistenznpc" -> "resistenznpc"
        "superresistenznpc" -> "superresistenznpc"
        else -> "npc"
    }
}

private fun parseInt(templateValue: String?): Int {
    val preprocessed = (templateValue ?: "0").replace(".", "")
    return Integer.parseInt(Regex("\\D*?(-?\\d+)[\\D\\d]*").find(preprocessed)?.groupValues?.get(1) ?: "0")
}

private fun parseLinkList(templateValue: String?, preferLinkText: Boolean): List<String> {
    val link = Regex("\\A[\\s\\S]*?\\[\\[([^|]*?)(\\|(.*?))?]]")

    return (templateValue ?: "").split("*").stream()
            .filter { it.contains(link) }
            .map {
                if (preferLinkText && link.find(it)!!.groups[2] != null) {
                    link.find(it)!!.groupValues[3]
                } else {
                    link.find(it)!!.groupValues[1]
                }
            }
            .collect(Collectors.toList())
}
